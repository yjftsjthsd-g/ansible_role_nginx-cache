nginx-cache
=========

Installs nginx and configures it to act as a caching proxy. Super useful if
you're doing a lot of rebuilds with containers or VMs and don't feel like going
out to the public internet to download packages over and over.


Role Variables
--------------

`nginx_cache_owner` - Sets user to own cache directory; as a user you hopefully shuoldn't care, but this is important for supporting different platforms.

`nginx_proxy_cache_valid` - How long to keep cached files. Defaults to 20 days because I was using it for package files where freshness wasn't important.

`nginx_proxy_cache_upstreams` - List of upstreams to proxy; ex. `- { name: alpine, url: http://dl-2.alpinelinux.org/alpine/ }` will create a `/alpine` path that points to a Alpine Linux repo.


Example Playbook
----------------

```
    - hosts: servers
      roles:
         - { role: ansible_role_nginx-cache }
```


License
-------

BSD-3-Clause


